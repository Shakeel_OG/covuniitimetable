package com.shakeel.covuniitimetable;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import static android.widget.Toast.LENGTH_SHORT;

public class BuildingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_building);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        spinnerBuilding();
        Spinner spnBuilding = (Spinner) findViewById(R.id.DropdownBuilding);
        spnBuilding.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setBuildingInfo();
            }

            // to close the onItemSelected
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void spinnerBuilding() {
        Spinner dropdown = (Spinner) findViewById(R.id.DropdownBuilding);
        String[] items = new String[]{"Alan Berry", "Armstrong Siddeley", "Bugatti", "Charles Ward", "Engineering and Computing Building",
                "Ellen Terry", "Frederick Lanchester Library", "George Eliot", "Graham Sutherland", "TheHub", "Jaguar", "James Starley", "Maurice Foss",
                "Priory Building", "Richard Crossman", "Sir John Laing", "Sir William Lyons", "Student Centre", "Whitefriars", "William Morris"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);
    }

    public void setBuildingInfo() {
        Spinner spnBuilding = (Spinner) findViewById(R.id.DropdownBuilding);
        String strBuildingName = spnBuilding.getSelectedItem().toString();
        String strBuildingInfo = strBuildingName.replace(" ", "_");
        TextView Name = (TextView) findViewById(R.id.txtBuildingName);
        TextView Info = (TextView) findViewById(R.id.txtBuildingInfo);
        Name.setText(strBuildingName);
        Info.setText(getResources().getString(getStringResourceByName(strBuildingInfo)));
    }

    private int getStringResourceByName(String strBuildingName) {
        //gets the information from String resource
        String packageName = "com.shakeel.covuniitimetable";
        int resId = getResources().getIdentifier(strBuildingName, "string", packageName);
        return resId;
    }


}
