package com.shakeel.covuniitimetable;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    RadioGroup rdoGroup;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(getResources().getText(R.string.main_activity_title));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DayOfWeek();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        RadioGroup radioGroupDays = (RadioGroup) findViewById(R.id.rdoGroup);
        radioGroupDays.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                onRadioGroupChange();
            }
        });
    }

    public void DayOfWeek() {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);
        switch (dayOfTheWeek.toLowerCase()) {
            case "monday":
                RadioButton rdoMon = (RadioButton) findViewById(R.id.rdoBtnMon);
                rdoMon.setChecked(true);
                sharedPreferences = getSharedPreferences("MySharedPrefsMon", Context.MODE_PRIVATE);
                break;
            case "tuesday":
                RadioButton rdoTue = (RadioButton) findViewById(R.id.rdoBtnTue);
                rdoTue.setChecked(true);
                sharedPreferences = getSharedPreferences("MySharedPrefsTue", Context.MODE_PRIVATE);
                break;
            case "wednesday":
                RadioButton rdoWed = (RadioButton) findViewById(R.id.rdoBtnWed);
                rdoWed.setChecked(true);
                sharedPreferences = getSharedPreferences("MySharedPrefsWed", Context.MODE_PRIVATE);
                break;
            case "thursday":
                RadioButton rdoThu = (RadioButton) findViewById(R.id.rdoBtnThu);
                rdoThu.setChecked(true);
                sharedPreferences = getSharedPreferences("MySharedPrefsThu", Context.MODE_PRIVATE);
                break;
            case "friday":
                RadioButton rdoFri = (RadioButton) findViewById(R.id.rdoBtnFri);
                rdoFri.setChecked(true);
                sharedPreferences = getSharedPreferences("MySharedPrefsFri", Context.MODE_PRIVATE);
                break;
            default:
                RadioButton rdoDef = (RadioButton) findViewById(R.id.rdoBtnMon);
                rdoDef.setChecked(true);
                sharedPreferences = getSharedPreferences("MySharedPrefsMon", Context.MODE_PRIVATE);
                break;
        }
        int[] textIDs = new int[]{R.id.time8, R.id.time9, R.id.time10, R.id.time11, R.id.time12, R.id.time13, R.id.time14, R.id.time15
                , R.id.time16, R.id.time17, R.id.time18, R.id.time19};
        TextView TextViewEntry;
        for (int i = 0; i < textIDs.length; i++) {
            TextViewEntry = (TextView) findViewById(textIDs[i]);
            TextViewEntry.setText(sharedPreferences.getString(String.valueOf(i + 8), ""));
        }
//        Snackbar.make(v, dayOfTheWeek, Snackbar.LENGTH_SHORT).setAction("Action", null).show();
    }

    public void onRadioGroupChange() {
        rdoGroup = (RadioGroup) findViewById(R.id.rdoGroup);
        String strDay = ((RadioButton) this.findViewById(rdoGroup.getCheckedRadioButtonId())).getText().toString();
        sharedPreferences = getSharedPreferences("MySharedPrefs" + strDay, Context.MODE_PRIVATE);
        int[] textIDs = new int[]{R.id.time8, R.id.time9, R.id.time10, R.id.time11, R.id.time12, R.id.time13, R.id.time14, R.id.time15
                , R.id.time16, R.id.time17, R.id.time18, R.id.time19};
        TextView editTextEntry;
        for (int i = 0; i < textIDs.length; i++) {
            editTextEntry = (TextView) findViewById(textIDs[i]);
            editTextEntry.setText(sharedPreferences.getString(String.valueOf(i + 8), ""));
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        onRadioGroupChange();
        super.onResume();
        ;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_editTimeTable) {
            Intent intent = new Intent(MainActivity.this, EditTimetable.class);
            startActivity(intent);
        } else if (id == R.id.nav_sem1) {

        } else if (id == R.id.nav_sem2) {
            Intent intent = new Intent(MainActivity.this, SemesterActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_building) {
            Intent intent = new Intent(MainActivity.this, BuildingActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_onlineTT) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://webapp.coventry.ac.uk/Timetable-main/Timetable/Current#view=agendaDay"));
            startActivity(intent);
        } else if (id == R.id.nav_share) {
            // gets the selected day
            String rdoText = ((RadioButton) this.findViewById(rdoGroup.getCheckedRadioButtonId())).getText().toString();
            String strShare = "My TimeTable for " + rdoText + " is:\n";
            //uses an array rather than a very large switch case/if statement
            int[] textIDs = new int[]{R.id.time8, R.id.time9, R.id.time10, R.id.time11, R.id.time12, R.id.time13, R.id.time14, R.id.time15
                    , R.id.time16, R.id.time17, R.id.time18, R.id.time19};
            TextView TextViewEntry;
            for (int i = 0; i < textIDs.length; i++) {
                TextViewEntry = (TextView) findViewById(textIDs[i]);
                //gets information from the textview rather than sharedprefs, makes sure it has a value before printing
                if (TextViewEntry.getText().toString() != "") {
                    // i+= 8 so that it will match the time thats displayes (if i is 0 it is 8am)
                    strShare += +(i + 8) + ".00:\n" + TextViewEntry.getText().toString() + "\n";
                }
            }
            //uses intent similar to adding extra information to an Activity
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, strShare);
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, "Send TimeTable To..."));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
