package com.shakeel.covuniitimetable;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EditTimetable extends AppCompatActivity {
    RadioGroup rdoGroup;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_timetable);
        //setting the back button
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        DayOfWeek();
        spinnerStart();
        spinnerEnd();
        Button btnDelete = (Button) findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                deleteAlert();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void DayOfWeek() {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);
        switch (dayOfTheWeek.toLowerCase()) {
            case "monday":
                RadioButton rdoMon = (RadioButton) findViewById(R.id.rdoBtnMon2);
                rdoMon.setChecked(true);
                break;
            case "tuesday":
                RadioButton rdoTue = (RadioButton) findViewById(R.id.rdoBtnTue2);
                rdoTue.setChecked(true);
                break;
            case "wednesday":
                RadioButton rdoWed = (RadioButton) findViewById(R.id.rdoBtnWed2);
                rdoWed.setChecked(true);
                break;
            case "thursday":
                RadioButton rdoThu = (RadioButton) findViewById(R.id.rdoBtnThu2);
                rdoThu.setChecked(true);
                break;
            case "friday":
                RadioButton rdoFri = (RadioButton) findViewById(R.id.rdoBtnFri2);
                rdoFri.setChecked(true);
                break;
        }
    }

    public void save(View v) {
        //getting the correct semester
        String strSem = "";
        rdoGroup = (RadioGroup) findViewById(R.id.rdoGroupSemester);
        String strSemcheck = ((RadioButton) this.findViewById(rdoGroup.getCheckedRadioButtonId())).getText().toString();
        switch (((RadioButton) this.findViewById(rdoGroup.getCheckedRadioButtonId())).getText().toString()) {
            case "Semester 2":
                strSem = "sem2";
                break;
            default:
                strSem = "";
        }
        //getting information from user input
        Spinner spnStart = (Spinner) findViewById(R.id.DropdownStart);
        Spinner spnHours = (Spinner) findViewById(R.id.DropdownHours);
        //setting variables for sharedprefs
        String time_key;
        EditText txtModule = (EditText) findViewById(R.id.txtModule);
        EditText txtRoom = (EditText) findViewById(R.id.txtRoom);
        EditText txtTeacher = (EditText) findViewById(R.id.txtTeacher);
        // getting rid of the date format in order to calculate
        String startStr = spnStart.getSelectedItem().toString().substring(0, spnStart.getSelectedItem().toString().length() - 3);
        int startInt = Integer.parseInt(startStr);
        int hoursInt = Integer.parseInt(spnHours.getSelectedItem().toString());
        //setting up shared prefs file for the correct date selected
        rdoGroup = (RadioGroup) findViewById(R.id.rdoGroup2);
        String strDay = ((RadioButton) this.findViewById(rdoGroup.getCheckedRadioButtonId())).getText().toString();
        sharedPreferences = getSharedPreferences("MySharedPrefs" + strDay, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        //in case of a 2 hour lecture
        while (hoursInt >= 1 && startInt < 20) {
            time_key = String.valueOf(startInt);
            hoursInt--;
            startInt++;
            if (((EditText) findViewById(R.id.txtModule)).getText().toString().isEmpty() == false || ((EditText) findViewById(R.id.txtRoom)).getText().toString().isEmpty() == false) {
                editor.putString(strSem + time_key, "Module: " + txtModule.getText().toString() + "\nRoom: " + txtRoom.getText().toString()
                        + "\nTeacher: " + txtTeacher.getText().toString());
                //displays the toast only once
                if (hoursInt == 0) {
                    Toast.makeText(v.getContext(), "Saved Successfully", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (hoursInt == 0) {
                    Toast.makeText(v.getContext(), "Please input Module and Room", Toast.LENGTH_SHORT).show();
                }
            }
            editor.apply();
        }
    }

    public void delete() {
        //getting the correct semester
        String strSem = "";
        rdoGroup = (RadioGroup) findViewById(R.id.rdoGroupSemester);
        String strSemcheck = ((RadioButton) this.findViewById(rdoGroup.getCheckedRadioButtonId())).getText().toString();
        switch (((RadioButton) this.findViewById(rdoGroup.getCheckedRadioButtonId())).getText().toString()) {
            case "Semester 2":
                strSem = "sem2";
                break;
            default:
                strSem = "";
        }
        //getting information from user input
        Spinner spnStart = (Spinner) findViewById(R.id.DropdownStart);
        Spinner spnHours = (Spinner) findViewById(R.id.DropdownHours);
        //setting variables for sharedprefs
        String time_key;
        // getting rid of the date format in order to calculate
        String startStr = spnStart.getSelectedItem().toString().substring(0, spnStart.getSelectedItem().toString().length() - 3);
        int startInt = Integer.parseInt(startStr);
        int hoursInt = Integer.parseInt(spnHours.getSelectedItem().toString());
        //setting up shared prefs file for the correct date selected
        rdoGroup = (RadioGroup) findViewById(R.id.rdoGroup2);
        String strDay = ((RadioButton) this.findViewById(rdoGroup.getCheckedRadioButtonId())).getText().toString();
        sharedPreferences = getSharedPreferences("MySharedPrefs" + strDay, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        //in case of a 2 hour lecture
        while (hoursInt >= 1 && startInt < 20) {
            time_key = String.valueOf(startInt);
            hoursInt--;
            startInt++;
            editor.putString(strSem + time_key, "");
            Toast.makeText(EditTimetable.this, "TimeTable deleted for " + time_key + ":00", Toast.LENGTH_SHORT).show();
            editor.apply();
        }
    }

    public void spinnerStart() {
        Spinner dropdown = (Spinner) findViewById(R.id.DropdownStart);
        String[] items = new String[]{"08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);
    }

    public void spinnerEnd() {
        Spinner dropdown = (Spinner) findViewById(R.id.DropdownHours);
        String[] items = new String[]{"1", "2"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);
    }

    public void deleteAlert() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE: // Yes button clicked
                        delete();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE: // No button clicked
                        break;
                }
            }
        };

        Spinner spnStart = (Spinner) findViewById(R.id.DropdownStart);
        Spinner spnHours = (Spinner) findViewById(R.id.DropdownHours);
        String strStart = spnStart.getSelectedItem().toString();
        String strHours = spnHours.getSelectedItem().toString();
        rdoGroup = (RadioGroup) findViewById(R.id.rdoGroup2);
        String strDay = ((RadioButton) this.findViewById(rdoGroup.getCheckedRadioButtonId())).getText().toString();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete the " + strHours + " hour session at " + strStart + " on " + strDay + "?")
                .setPositiveButton("Yes, delete it!", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}





